//
//  RegisterNewUserVC_AuthType_01.swift
//  Tied
//
//  Created by Jan Potuznik on 02.06.16.
//  Copyright © 2016 donAFRO. All rights reserved.
//

import UIKit


class RegisterNewUserVC_AuthType_01: RegisterParentClassViewController, UITextFieldDelegate {

    var bLoadedView: Bool = false
    var fErrorViewHeight: CGFloat = 0
    
    @IBOutlet weak var m_txtEmail: UITextField!
    
    //constraint to move layout for keyboard
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(keyboardWillAppear(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(keyboardWillDisappear(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        
        self.m_txtEmail.delegate = self
        m_txtEmail.becomeFirstResponder()
        
        
        
        
    }
    
    
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if (self.bLoadedView) {
            return
        }
        
        bLoadedView = true
        
        makeUserInterface()
    }
    
    
    
    
    func makeUserInterface() {
//        self.m_lblSignUpProcessNo.text = "1/\(Constants.Total_SingUpProcess_Count)"
//        self.m_lblSignUpProcessNo.text = ""
        
    }
    
    
    
    @IBAction func continueButton(sender: UIButton) {

        if let email = m_txtEmail.text {
            
            if email.characters.count < 5 {
                showErrorMessageWithText("Email too short")
                return
            }
            
            if !(email.containsString("@")) { // if it does not contain @
                showErrorMessageWithText("This aint email")
                return
            }
            
            
            //show next View
            
        }
        
        
    }
    
    
    
    // MARK: - Keyboard show/hide and resizing autolayout
    func keyboardWillAppear(notification: NSNotification) {
        _ = UIApplication.sharedApplication()
        if let userInfo = notification.userInfo {
            if let keyboardHeight = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size.height {
                bottomConstraint.constant = keyboardHeight
                UIView.animateWithDuration(0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func keyboardWillDisappear(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let _ = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size.height {
                bottomConstraint.constant = 0.0
                UIView.animateWithDuration(0.25, animations: { () -> Void in self.view.layoutIfNeeded() })
            }
        }
    }
    
    
    
}
