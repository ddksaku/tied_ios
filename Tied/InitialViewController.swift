//
//  ViewController.swift
//  Tied
//
//  Created by Admin on 12/05/16.
//  Copyright © 2016 DevStar. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    
    var bLoadedView = false
    var fPagedFlowViewHeight: CGFloat = 0
    var fPagedFlowViewWidth: CGFloat = 0
    let arrCard = ["card1", "card2", "card3", "card4"]
    
//    @IBOutlet weak var pageFlow: PagedFlowView!
//    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.edgesForExtendedLayout = .None;
        
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if (bLoadedView) {
            return
        }
        
        bLoadedView = true
        
//        fPagedFlowViewHeight = CGRectGetHeight(pageFlow.bounds)
//        fPagedFlowViewWidth = CGRectGetWidth(self.view.bounds)
//        
//        makeUserInterface()
    }
    
//    func makeUserInterface() {
//        self.pageControl.numberOfPages = arrCard.count
//        self.pageControl.pageIndicatorTintColor = Constants.Colors.PageControl_InActive_Color
//        self.pageControl.currentPageIndicatorTintColor = Constants.Colors.PageControl_Active_Color
//        
//        self.pageFlow.delegate = self;
//        self.pageFlow.dataSource = self;
//        self.pageFlow.minimumPageAlpha = 0.4;
//        self.pageFlow.minimumPageScale = 0.8;
//        self.pageFlow.orientation = PagedFlowViewOrientationHorizontal;
//    }

    @IBAction func actionRegister(sender: AnyObject) {
        self.performSegueWithIdentifier("showRegisterView", sender: nil)
    }

    @IBAction func actionSignIn(sender: AnyObject) {
        performSegueWithIdentifier("showSignInView", sender: nil)
    }
    
//    //MARK - PagedFlowViewDelegate and Data Source
//    func sizeForPageInFlowView(flowView: PagedFlowView!) -> CGSize {
//        return CGSizeMake(fPagedFlowViewWidth / 3.0 * 2.0, fPagedFlowViewHeight)
//    }
//    
//    func flowView(flowView: PagedFlowView!, didScroll index: Int) {
//        self.pageControl.currentPage = index
//    }
//    
//    func flowView(flowView: PagedFlowView!, didTapPageAtIndex index: Int) {
//        self.pageControl.currentPage = index
//    }
//    
//    func flowView(flowView: PagedFlowView!, didEndScroll index: Int) {
//        self.pageControl.currentPage = index
//    }
//    
//    func flowView(flowView: PagedFlowView!, didScrollToPageAtIndex index: Int) {
//        self.pageControl.currentPage = index
//    }
//    
//    func flowView(flowView: PagedFlowView!, cellForPageAtIndex index: Int) -> UIView! {
//        var imageView = flowView.dequeueReusableCell() as? UIImageView
//        
//        if (imageView == nil) {
//            imageView = UIImageView()
//            imageView?.contentMode = .ScaleAspectFit
//            imageView?.bounds = CGRectMake(0, 0, fPagedFlowViewWidth, fPagedFlowViewHeight)
//        }
//        
//        imageView?.clipsToBounds = true
//        imageView?.layer.masksToBounds = true
//        imageView?.image = UIImage(named: arrCard[index])
//        return imageView
//    }
//    
//    func numberOfPagesInFlowView(flowView: PagedFlowView!) -> Int {
//        return arrCard.count
//    }
    
}

