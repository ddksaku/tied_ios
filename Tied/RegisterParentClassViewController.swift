//
//  RegisterParentClassViewController.swift
//  Tied
//
//  Created by Jan Potuznik on 04.06.16.
//  Copyright © 2016 donAFRO. All rights reserved.
//

import UIKit

class RegisterParentClassViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    
    
    //MARK: - Show error messages
    var lab = UILabel()
    
    func showErrorMessageWithText(message: String) {
        
        let tempText = self.title! // store the current title to temp var
        self.title! = message
        
        //create label over the navigation bar to display the red color
        let statusHeight = UIApplication.sharedApplication().statusBarFrame.size.height
        let navHeight = self.navigationController!.navigationBar.frame.size.height
        lab = UILabel(frame: CGRectMake(0,0, self.view.frame.size.width, navHeight + statusHeight))
        lab.backgroundColor = UIColor.redColor()
        self.view.addSubview(lab)
        
        
        //reset the warning after some time to original design layout
        let _ = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: #selector(hideTheErrorLabel), userInfo: tempText, repeats: false)
    }
    
    
    func hideTheErrorLabel(timer: NSTimer) {
        if let tt = timer.userInfo as? String {
            self.title = tt
        }
        
        lab.removeFromSuperview()
        
    }
    
    
    
    

}
