//
//  SignInViewController.swift
//  Tied
//
//  Created by Jan Potuznik on 02.06.16.
//  Copyright © 2016 donAFRO. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var m_txtEmailAddress: UITextField!
    @IBOutlet weak var m_lblEmailAddressIndicator: UILabel!
    
    @IBOutlet weak var m_lblPasswordIndicator: UILabel!
    @IBOutlet weak var m_txtPassword: UITextField!
    
    
    //unwind segue
    @IBAction func unwindToSegue (segue : UIStoryboardSegue) {
        print("doing the unwind")
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.edgesForExtendedLayout = .None
        makeUserInterface()
        
        
    }

    
    func makeUserInterface() {
        self.m_txtEmailAddress.delegate = self
        self.m_txtPassword.delegate = self
        
        self.m_txtEmailAddress.addTarget(self, action: #selector(SignInViewController.textChanged(_:)), forControlEvents: .EditingChanged)
        self.m_txtPassword.addTarget(self, action: #selector(SignInViewController.textChanged(_:)), forControlEvents: .EditingChanged)
        
        self.m_lblEmailAddressIndicator.hidden = true
        self.m_lblPasswordIndicator.hidden = true
    }
    
    func hideKeyboard() {
        self.m_txtEmailAddress.resignFirstResponder()
        self.m_txtPassword.resignFirstResponder()
    }
    
    func textChanged(textField: UITextField) {
        print("changed")
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        hideKeyboard()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if (textField == self.m_txtEmailAddress) {
            self.m_lblEmailAddressIndicator.hidden = false
        } else if (textField == self.m_txtPassword) {
            self.m_lblPasswordIndicator.hidden = false
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if (textField == self.m_txtEmailAddress) {
            self.m_lblEmailAddressIndicator.hidden = true
        } else if (textField == self.m_txtPassword) {
            self.m_lblPasswordIndicator.hidden = true
        }
    }
    
    
    //MARK: - butons
    @IBAction func signInButton(sender: UIButton) {
    
    }
    
    
    //show register new user view
    @IBAction func signUpButton(sender: UIButton) {
        self.performSegueWithIdentifier("showRegisterView", sender: nil)
    }
    
    //facebook auth
    @IBAction func facebookButton(sender: UIButton) {
    
    }
    
    //twitter
    @IBAction func twitterButton(sender: UIButton) {
        
    }
    
    

}
