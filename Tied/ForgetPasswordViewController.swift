//
//  ForgetPasswordViewController.swift
//  Tied
//
//  Created by Jan Potuznik on 02.06.16.
//  Copyright © 2016 donAFRO. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController, UITextFieldDelegate {

    
    @IBOutlet weak var m_txtEmailAddress: UITextField!
    
    @IBOutlet weak var m_lblEmailIndicator: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.edgesForExtendedLayout = .None
        
        makeUserInterface()
    }
    
    func makeUserInterface() {
        self.m_txtEmailAddress.delegate = self
        
        self.m_txtEmailAddress.addTarget(self, action: #selector(SignInViewController.textChanged(_:)), forControlEvents: .EditingChanged)
        
        self.m_lblEmailIndicator.hidden = true
        
        
        
    }
    
    func hideKeyboard() {
        self.m_txtEmailAddress.resignFirstResponder()
    }
    
    func textChanged(textField: UITextField) {
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        hideKeyboard()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if (textField == self.m_txtEmailAddress) {
            self.m_lblEmailIndicator.hidden = false
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if (textField == self.m_txtEmailAddress) {
            self.m_lblEmailIndicator.hidden = true
        }
    }
    
    
    @IBAction func resetPasswordButton(sender: UIButton) {
        //doing reseting password
        
        self.performSegueWithIdentifier("showForgotPassSuccessView", sender: nil)
    }

}
