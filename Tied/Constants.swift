//
//  Constants.swift
//  Tied
//
//  Created by Jan Potuznik on 02.06.16.
//  Copyright © 2016 donAFRO. All rights reserved.
//

import Foundation
import UIKit


public class Constants {
    
    static let Total_SingUpProcess_Count = 16
    static let Verification_Code_length = 6
    
    struct Colors {
        static let PageControl_InActive_Color = UIColor(red: 29, green: 29, blue: 38, alpha: 0.1)
        static let PageControl_Active_Color = UIColor(red: 60, green: 150, blue: 255, alpha: 1)
        static let InActive_Color = UIColor(red: 255, green: 255, blue: 255, alpha: 0.5)
        static let Active_Color = UIColor.whiteColor()
    }
    
    struct MainFontNames {
        static let Regular = "BrandonGrotesque-Bold"
        static let Medium = "BrandonGrotesque-Medium"
        static let Bold = "BrandonGrotesque-Bold"
    }
    
    struct Avatar {
        static let Default = "default_avatar"
    }
    
    struct DotCheck {
        static let UnCheck = "dot_unchecked_icon"
        static let Check = "dot_checked_icon"
    }
    
    struct Check1 {
        static let UnCheck = "fill_unchecked_icon"
        static let Check = "checked_icon"
    }
    
    struct CellID {
        static let TerritoryCell = "TerritoryCell"
        static let IndustryCell = "IndustryCell"
    }
    
    struct RepType {
        static let CaseCount = 2
        static let Rep_Self = 0
        static let Rep_Group = 1
    }
    
    struct Principle {
        static let CaseCount = 2
        static let Principal = 0
        static let SupRep = 1
    }
    
    struct  Invite {
        static let CaseCount = 3
        static let SMS = 0
        static let Email = 1
        static let NoInvite = 2
    }
    
    struct CoWorker {
        static let CaseCount = 4
        static let OneToFive = 0
        static let SixToTen = 1
        static let ElevenToTwenty = 2
        static let FiftyMore = 3
    }
    
    struct DeviceInfo {
        static let DefaultDeviceToken = "2222222"
        static let DeviceType = "ios"
    }
    
    struct WebServiceApi {
        static let ApiBaseUrl = "http://tied.goattale.com:8100/api"
    }
    
}